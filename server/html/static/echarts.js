const ECharts = {
    template: `<div ref="chart" style="width: 100%; height:80px;" :style="{height: + height + 'px'}"></div>`,
    props: {
        option: {
            type: Object,
            default: {}
        },
    },
    data() {
        return {
            height: 80,
            chart: null,
            handleResize: null,
        }
    },
    watch: {
        option: {
            // deep: true,
            handler(val) {
                if(!this.chart) {
                    return;
                }
                this.chart.setOption(val);
            }
        }
    },
    mounted() {
        console.log('echarts mounted');
        this.clacSize();
        this.$nextTick(() => {
            this.init();
        });
    },
    unmounted() {
        console.log('echarts unmounted');
        this.chart && this.chart.dispose();
        this.chart = null;
        if(this.handleResize) {
            window.removeEventListener('resize', this.handleResize);
        }
    },
    methods: {
        init() {
            if(typeof(echarts) === 'undefined') {
                if(document.getElementById('echarts-js')) {
                    setTimeout(() => {
                        this.init();
                    }, 100);
                    return;
                }
                console.log('load echarts js');
                const script = document.createElement('script');
                script.setAttribute('id', 'echarts-js');
                script.src = '/static/echarts.min.js';
                script.onload = () => {
                    console.log('echarts js loaded');
                    this.init();
                }
                document.body.appendChild(script);
                return;
            }
            this.chart = Vue.markRaw(echarts.init(this.$refs.chart));
            this.chart.setOption(this.option);
            if(!this.handleResize) {
                this.handleResize = () => {
                    if(!this.$refs.chart) {
                        return;
                    }
                    this.clacSize();
                    this.$nextTick(() => {
                        this.chart.resize();
                    });
                }
            }
            window.addEventListener('resize', this.handleResize);
        },
        clacSize() {
            if(!this.$refs.chart) {
                return;
            }
            let width = this.$refs.chart.offsetWidth;
            let height = Math.round(width * 0.8);
            if(this.option.height && this.option.width) {
                height = Math.round(this.option.height / this.option.width * width);
                if(width > this.option.width && height > this.option.height) {
                    width = this.option.width;
                    height = this.option.height;
                }
            }
            if(height > window.innerHeight) {
                height = window.innerHeight;
            }
            this.width = width;
            this.height = height;
        },
    }
};